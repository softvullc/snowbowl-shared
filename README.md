
## Shared code for SeniorVu projects

### Contributing
1. Add code to *src*
2. `npm run build`
3. `git add -A && git commit`
4. `npm version patch`
6. `git push --follow-tags`
7. Update the version is `package.json` in `snowbowl` and `snowbowl-design`
8. Run `npm install` in each package, make sure to `nvm use` first so you get the right node version.
9. Patch and deploy new versions of `snowbowl` and `snowbowl-design`
