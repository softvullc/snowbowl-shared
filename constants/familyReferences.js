/**
* THIS IS A COMPILED FILE. DO NOT MODIFY.
**/'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = [{
  id: 'Daughter',
  name: 'Daughter'
}, {
  id: 'Son',
  name: 'Son'
}, {
  id: 'Grandchild',
  name: 'Grandchild'
}, {
  id: 'Husband',
  name: 'Husband'
}, {
  id: 'Wife',
  name: 'Wife'
}, {
  id: 'Father',
  name: 'Father'
}, {
  id: 'Mother',
  name: 'Mother'
}, {
  id: 'Brother',
  name: 'Brother'
}, {
  id: 'Sister',
  name: 'Sister'
}, {
  id: 'Niece',
  name: 'Niece'
}, {
  id: 'Nephew',
  name: 'Nephew'
}, {
  id: 'Friend',
  name: 'Friend'
}, {
  id: 'Social Worker/Case Manager',
  name: 'Social Worker/Case Manager'
}, {
  id: 'POA/Guardian',
  name: 'POA/Guardian'
}, {
  id: 'Daughter-in-law',
  name: 'Daughter-in-law'
}, {
  id: 'Son-in-law',
  name: 'Son-in-law'
}, {
  id: 'Sister-in-law',
  name: 'Sister-in-law'
}, {
  id: 'Brother-in-law',
  name: 'Brother-in-law'
}, {
  id: 'Neighbor',
  name: 'Neighbor'
}, {
  id: 'Cousin',
  name: 'Cousin'
}, {
  id: 'Ex-Spouse',
  name: 'Ex-Spouse'
}, {
  id: 'Other',
  name: 'Other'
}];