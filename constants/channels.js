/**
* THIS IS A COMPILED FILE. DO NOT MODIFY.
**/'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = [{
  id: 'Email',
  name: 'Email'
}, {
  id: 'Direct Mail',
  name: 'Direct Mail'
}, {
  id: 'Digital',
  name: 'Digital'
}, {
  id: 'Other',
  name: 'Other'
}];