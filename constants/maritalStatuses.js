/**
* THIS IS A COMPILED FILE. DO NOT MODIFY.
**/'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = [{
  id: 'Married',
  name: 'Married'
}, {
  id: 'Divorced',
  name: 'Divorced'
}, {
  id: 'Separated',
  name: 'Separated'
}, {
  id: 'Single',
  name: 'Single'
}, {
  id: 'Widowed',
  name: 'Widowed'
}, {
  id: 'Domestic Partner',
  name: 'Domestic Partner'
}];