/**
* THIS IS A COMPILED FILE. DO NOT MODIFY.
**/'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.taskDescriptionsIndex = exports.statesIndex = exports.maritalStatusesIndex = exports.familyReferencesIndex = exports.eventTypesIndex = exports.channelsIndex = exports.taskDescriptions = exports.states = exports.maritalStatuses = exports.familyReferences = exports.eventTypes = exports.channels = undefined;

var _channels = require('./channels');

var _channels2 = _interopRequireDefault(_channels);

var _eventTypes = require('./eventTypes');

var _eventTypes2 = _interopRequireDefault(_eventTypes);

var _familyReferences = require('./familyReferences');

var _familyReferences2 = _interopRequireDefault(_familyReferences);

var _maritalStatuses = require('./maritalStatuses');

var _maritalStatuses2 = _interopRequireDefault(_maritalStatuses);

var _states = require('./states');

var _states2 = _interopRequireDefault(_states);

var _taskDescriptions = require('./taskDescriptions');

var _taskDescriptions2 = _interopRequireDefault(_taskDescriptions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var keyBy = function keyBy(list, key) {
  return list.reduce(function (index, item) {
    index[item[key]] = item;

    return index;
  }, {});
};

var channelsIndex = keyBy(_channels2.default, 'id');
var eventTypesIndex = keyBy(_eventTypes2.default, 'code');
var familyReferencesIndex = keyBy(_familyReferences2.default, 'id');
var maritalStatusesIndex = keyBy(_maritalStatuses2.default, 'id');
var statesIndex = keyBy(_states2.default, 'id');
var taskDescriptionsIndex = keyBy(_taskDescriptions2.default, 'code');

exports.channels = _channels2.default;
exports.eventTypes = _eventTypes2.default;
exports.familyReferences = _familyReferences2.default;
exports.maritalStatuses = _maritalStatuses2.default;
exports.states = _states2.default;
exports.taskDescriptions = _taskDescriptions2.default;
exports.channelsIndex = channelsIndex;
exports.eventTypesIndex = eventTypesIndex;
exports.familyReferencesIndex = familyReferencesIndex;
exports.maritalStatusesIndex = maritalStatusesIndex;
exports.statesIndex = statesIndex;
exports.taskDescriptionsIndex = taskDescriptionsIndex;