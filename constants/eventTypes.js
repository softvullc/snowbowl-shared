/**
* THIS IS A COMPILED FILE. DO NOT MODIFY.
**/'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = [{
  code: 'formFill',
  name: 'Form Fill',
  title: 'Form Fill',
  description: function description() {
    return 'A form fill was posted';
  },
  icon: 'edit',
  show: true, // determines if the event is shown to the user in various components
  external: true // can external sources create this event type via /v1/activity
}, {
  code: 'add',
  name: 'Created',
  title: 'Created',
  description: function description(_ref) {
    var userName = _ref.userName,
        name = _ref.name;
    return userName + ' created <b>' + name + '</b>';
  },
  icon: 'plus',
  show: true,
  external: false
}, {
  code: 'add.formFill',
  name: 'Created From Form Fill',
  title: 'Created From Form Fill',
  description: function description(_ref2) {
    var userName = _ref2.userName,
        name = _ref2.name;
    return userName + ' created <b>' + name + '</b> via form fill';
  },
  icon: 'plus',
  show: true,
  external: false
}, {
  code: 'purchase',
  name: 'Purchased',
  title: 'Purchased',
  description: function description(_ref3) {
    var userName = _ref3.userName,
        name = _ref3.name;
    return userName + ' purchased <b>' + name + '</b>';
  },
  icon: 'plus',
  show: true,
  external: false
}, {
  code: 'note',
  name: 'Note Added',
  title: 'Note Added',
  description: function description(_ref4) {
    var userName = _ref4.userName,
        name = _ref4.name;
    return userName + ' created a note for ' + name;
  },
  icon: 'file-text-o',
  show: true,
  external: true
}, {
  code: 'call',
  name: 'Call Logged',
  title: 'Call Logged',
  description: function description(_ref5) {
    var userName = _ref5.userName,
        name = _ref5.name;
    return userName + ' logged a call to ' + name;
  },
  icon: 'phone',
  show: true,
  external: true
}, {
  code: 'call.queued',
  name: 'Call Logged',
  title: 'Call Logged',
  description: function description(_ref6) {
    var userName = _ref6.userName,
        name = _ref6.name;
    return userName ? userName + ' queued a call to ' + name : 'A call was queued for ' + name;
  },
  icon: 'phone',
  show: true,
  external: false
}, {
  code: 'call.recording',
  name: 'Call Recorded',
  title: 'Call Recorded',
  description: function description() {
    return 'A call was recorded';
  },
  icon: 'phone',
  show: true,
  external: false
}, {
  code: 'email',
  name: 'Email Logged',
  title: 'Email Logged',
  description: function description(_ref7) {
    var userName = _ref7.userName,
        name = _ref7.name;
    return userName + ' logged an email to ' + name;
  },
  icon: 'envelope',
  show: true,
  external: true
}, {
  code: 'softvu.lead.posted',
  name: 'Lead Posted',
  title: 'Lead Posted',
  description: function description(_ref8) {
    var userName = _ref8.userName,
        name = _ref8.name;
    return (userName ? userName + ' posted' : 'Posted') + ' <b>' + name + '</b> to SoftVu';
  },
  icon: 'envelope',
  show: false,
  external: false
},
// TODO: Delete the following after migrating email.created events to softvu.lead.posted
{
  code: 'email.created',
  name: 'Lead Posted',
  title: 'Lead Posted',
  description: function description(_ref9) {
    var userName = _ref9.userName,
        name = _ref9.name;
    return (userName ? userName + ' posted' : 'Posted') + ' <b>' + name + '</b> to SoftVu';
  },
  icon: 'envelope',
  show: false,
  external: false
}, {
  code: 'email.queued',
  name: 'Email Prepared',
  title: 'Email Prepared',
  description: function description(_ref10) {
    var name = _ref10.name;
    return 'An email has been queued for delivery to <b>' + name + '</b>';
  },
  icon: 'envelope',
  show: true,
  external: false
}, {
  code: 'email.delivered',
  name: 'Email Delivered',
  title: 'Email Delivered',
  description: function description(_ref11) {
    var name = _ref11.name;
    return 'An email has been delivered to <b>' + name + '</b>';
  },
  icon: 'envelope',
  show: true,
  external: false
}, {
  code: 'email.bounced',
  name: 'Email Bounced',
  title: 'Email Bounced',
  description: function description() {
    return 'An email has bounced';
  },
  icon: 'envelope',
  show: true,
  external: false
}, {
  code: 'email.opened',
  name: 'Email Opened',
  title: 'Email Opened',
  description: function description(_ref12) {
    var name = _ref12.name;
    return 'The email has been opened by <b>' + name + '</b>';
  },
  icon: 'envelope',
  show: true,
  external: false
}, {
  code: 'email.clicked',
  name: 'Email Clicked',
  title: 'Email Clicked',
  description: function description(_ref13) {
    var name = _ref13.name;
    return '<b>' + name + '</b> has clicked a link in the email';
  },
  icon: 'envelope',
  show: true,
  external: false
}, {
  code: 'softvu.lead.errored',
  name: 'SoftVu Error',
  title: 'SoftVu Error',
  description: function description(_ref14) {
    var name = _ref14.name;
    return 'Received an error posting <b>' + name + '</b> to SoftVu';
  },
  icon: 'envelope',
  show: true,
  external: false
},
// TODO: Delete the following after migrating email.create_error events to softvu.lead.errored
{
  code: 'email.create_error',
  name: 'SoftVu Error',
  title: 'SoftVu Error',
  description: function description(_ref15) {
    var name = _ref15.name;
    return 'Received an error posting <b>' + name + '</b> to SoftVu';
  },
  icon: 'envelope',
  show: false,
  external: false
}, {
  code: 'lead.alert',
  name: 'Lead Alert',
  title: 'Lead Alert',
  description: function description(_ref16) {
    var name = _ref16.name;
    return 'An alert for <b>' + name + '</b> was sent';
  },
  icon: 'envelope',
  show: true,
  external: false
}, {
  code: 'optout',
  name: 'Email Optout',
  title: 'Email Optout',
  description: function description(_ref17) {
    var name = _ref17.name;
    return '<b>' + name + '</b> has opted out from receiving emails';
  },
  icon: 'envelope',
  show: true,
  external: false
}, {
  code: 'tourScheduled',
  name: 'Tour Scheduled',
  title: 'Tour Scheduled',
  description: function description(_ref18) {
    var name = _ref18.name,
        date = _ref18.date,
        userName = _ref18.userName;
    return userName + ' scheduled ' + name + ' to tour on ' + date;
  },
  icon: 'calendar',
  show: true,
  external: false
}, {
  code: 'chatSession',
  name: 'Chat Session',
  title: 'Chat Session',
  description: function description(_ref19) {
    var name = _ref19.name;
    return '<b>' + name + '</b> participated in a chat session';
  },
  icon: 'comments-o',
  show: true,
  external: true
}, {
  code: 'call.inbound',
  name: 'Inbound Call',
  title: 'Inbound Call',
  description: function description(_ref20) {
    var name = _ref20.name;
    return 'An inbound call from <b>' + name + '</b> was logged';
  },
  icon: 'phone',
  show: true,
  external: true
}, {
  code: 'call.outbound',
  name: 'Outbound Call',
  title: 'Outbound Call',
  description: function description(_ref21) {
    var name = _ref21.name;
    return 'An outbound call from <b>' + name + '</b> was logged';
  },
  icon: 'phone',
  show: true,
  external: true
}, {
  code: 'community.accepted',
  name: 'Community Accepted',
  title: 'Community Accepted',
  description: function description(_ref22) {
    var name = _ref22.name;
    return 'An external source has posted <b>' + name + '</b> as <i>accepted</i>';
  },
  icon: 'check',
  show: true,
  external: true
}, {
  code: 'community.rejected',
  name: 'Community Rejected',
  title: 'Community Rejected',
  description: function description(_ref23) {
    var name = _ref23.name;
    return 'An external source has posted <b>' + name + '</b> as <i>rejected</i>';
  },
  icon: 'exclamation-triangle',
  show: true,
  external: true
}, {
  code: 'community.managed.change',
  name: 'Community "Managed" Change',
  title: 'Community "Managed" Change',
  description: function description(_ref24) {
    var name = _ref24.name,
        userName = _ref24.userName;

    return '<b>' + userName + '</b> changed <b>' + name + '</b> management';
  },
  icon: 'handshake-o',
  show: true,
  external: false
}, {
  code: 'community.change',
  name: 'Community Changed',
  title: 'Community Changed',
  description: function description(_ref25) {
    var username = _ref25.username;

    return 'Community was changed by ' + userName;
  },
  icon: 'file-text-o',
  show: true,
  external: true
}, {
  code: 'sms.queued',
  name: 'Text Message Queued',
  title: 'Text Message Queued',
  description: function description(_ref26) {
    var userName = _ref26.userName,
        data = _ref26.data;

    return '<b>' + userName + '</b> sent a text message to ' + data.recipients;
  },
  icon: 'commenting',
  eventClass: function eventClass(event) {
    if (['failed', 'undelivered'].includes(event.data.currentStatus)) return 'danger';

    return '';
  },

  // iconClass(event) {
  //   if (['failed', 'delivered'].includes(event.data.currentStatus)) return 'danger';

  //   return '';
  // },
  show: true,
  external: false
}, {
  code: 'sms.received',
  name: 'Text Message Received',
  title: 'Text Message Received',
  description: function description(_ref27) {
    var data = _ref27.data;

    return 'We received a text message from <b>' + data.sender + '</b>';
  },
  icon: 'commenting',
  show: true,
  external: false
}, {
  code: 'sms.optout',
  name: 'SMS Opt-out',
  title: 'SMS Opt-out',
  description: function description(_ref28) {
    var data = _ref28.data,
        user = _ref28.user,
        userName = _ref28.userName,
        name = _ref28.name;

    if (user) return '<b>' + userName + '</b> opted ' + name + ' out of text messages';

    return '<b>' + data.sender + '</b> opted out of text messages';
  },
  icon: 'commenting',
  show: true,
  external: false
}, {
  code: 'video.recording',
  name: 'Video Recording',
  title: 'Video Recording',
  description: function description(_ref29) {
    var data = _ref29.data,
        user = _ref29.user,
        userName = _ref29.userName,
        name = _ref29.name;

    return 'A video was recorded for <b>' + name + '</b>';
  },
  icon: 'video-camera',
  show: true,
  external: false
}, {
  code: 'alert.financial_advocates',
  name: 'Alert to Catalyst Financial Advocates',
  title: 'Alert to Catalyst Financial Advocates',
  description: function description(_ref30) {
    var name = _ref30.name;
    return 'An alert for <b>' + name + '</b> was sent to Catalyst Financial Advocates';
  },
  icon: 'envelope',
  show: true,
  external: false
}];