/**
* THIS IS A COMPILED FILE. DO NOT MODIFY.
**/'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var parse = exports.parse = function parse(s) {
  if (typeof s !== 'string') throw new Error('Cannot parse phone for type ' + (typeof s === 'undefined' ? 'undefined' : _typeof(s)));

  // strip out everything that's not a number
  return s.replace(/[^\d]/g, '');
};

var validate = exports.validate = function validate(s) {
  var parsedVal = parse(s);
  if (parsedVal.length !== 10 && parsedVal.length !== 11) throw new Error('Phone number must contain 10 or 11 digits');
};

var isValid = exports.isValid = function isValid(s) {
  try {
    validate(s);

    return true;
  } catch (e) {
    return false;
  }
};

var format = exports.format = function format(s) {
  if (isValid(s)) {
    var p = parse(s);

    if (p.length === 10) {
      var x = p.match(/^(\d{3})(\d{3})(\d{4})$/);

      // example: (816) 555-0000
      return '(' + x[1] + ') ' + x[2] + '-' + x[3];
    } else if (p.length === 11) {
      var y = p.match(/^(\d)(\d{3})(\d{3})(\d{4})$/);

      // example: 1 (816) 555-0000
      return y[1] + ' (' + y[2] + ') ' + y[3] + '-' + y[4];
    }
  } else if (typeof s === 'string') return s;

  return '';
};