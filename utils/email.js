/**
* THIS IS A COMPILED FILE. DO NOT MODIFY.
**/'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// process email into something more usable
var process = exports.process = function process(raw) {
  return typeof raw === 'string' ? raw.replace(/\s/g, '') : raw;
};

// throw errors if invalid email
var validate = exports.validate = function validate(email) {
  if (typeof email !== 'string') throw new Error('Email must be of type string, not ' + (typeof email === 'undefined' ? 'undefined' : _typeof(email)));

  // RFC 5322 - https://emailregex.com/
  if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
    throw new Error(email + ' is not a valid email address');
  }
};

// return boolean indicating email validity
var isValid = exports.isValid = function isValid(email) {
  try {
    validate(email);

    return true;
  } catch (e) {
    return false;
  }
};

// process email into something more usable, throw if it's invalid
var parse = exports.parse = function parse(email) {
  var clean = process(email);
  validate(clean);

  return clean;
};

// attempt to parse email, otherwise return null

exports.default = function (email) {
  try {
    return parse(email);
  } catch (e) {
    return null;
  }
};