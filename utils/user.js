/**
* THIS IS A COMPILED FILE. DO NOT MODIFY.
**/'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var getFullName = exports.getFullName = function getFullName() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      firstName = _ref.firstName,
      lastName = _ref.lastName;

  return [firstName, lastName].join(' ').trim();
};

var getNames = exports.getNames = function getNames() {
  var fullName = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

  var names = fullName.split(' ');
  var lastName = '';
  var firstName = '';
  if (names.length) lastName = names[names.length - 1];
  if (names.length > 1) firstName = names.slice(0, names.length - 1).join(' ');

  return { firstName: firstName, lastName: lastName };
};