/**
* THIS IS A COMPILED FILE. DO NOT MODIFY.
**/'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var locale = 'en-US';
var currencyType = 'USD';

// strings get stripped of anything that's not a number or decimal before attempted parse
var parse = exports.parse = function parse(c) {
  var parseFromString = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : Number.parseFloat;

  switch (typeof c === 'undefined' ? 'undefined' : _typeof(c)) {
    case 'string':
      {
        var parsedVal = parseFromString(c.replace(/[^\d.]/g, ''));
        if (isFinite(parsedVal)) return parsedVal;
        throw new Error(c + ' is not a valid currency value');
      }
    case 'number':
      return c;
    default:
      throw new Error('Cannot parse currency for type ' + (typeof c === 'undefined' ? 'undefined' : _typeof(c)));
  }
};

var validate = exports.validate = function validate(c) {
  var parseFromString = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : Number.parseFloat;

  parse(c, parseFromString); // do not return anything
};

var isValid = exports.isValid = function isValid(c) {
  var parseFromString = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : Number.parseFloat;

  try {
    parse(c, parseFromString);

    return true;
  } catch (e) {
    return false;
  }
};

var format = exports.format = function format(c) {
  var maximumFractionDigits = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
  var minimumFractionDigits = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 2;

  if (!isValid(c)) return '';

  return parse(c).toLocaleString(locale, {
    style: 'currency',
    currency: currencyType,
    maximumFractionDigits: maximumFractionDigits,
    minimumFractionDigits: minimumFractionDigits
  });
};