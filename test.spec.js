/* eslint import/no-duplicates: 0 */

import test from 'ava';
import descs from './src/constants/taskDescriptions';
import { unmanagedDescriptions } from './src/constants/taskDescriptions';

test('Default import is all description types', (t) => {
  t.deepEqual(descs, [
    'Call Lead',
    'Call Community',
    'Email Only',
    'Tour Reminder',
    'Tour Follow-up',
    'Send Brochure',
    'Other',
  ]);
});

test('Unmanaged desc import is missing call community desc type', (t) => {
  t.deepEqual(unmanagedDescriptions, [
    'Call Lead',
    'Email Only',
    'Tour Reminder',
    'Tour Follow-up',
    'Send Brochure',
    'Other',
  ]);
});
