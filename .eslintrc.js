module.exports = {
  root: true,
  extends: 'airbnb/base',
  env: {
    'node': true,
    'mocha': true,
  },
  rules: {
    'indent': ['error', 2, { 'SwitchCase': 1 , 'MemberExpression': 1 }],
    'newline-before-return': 'error',
    'no-shadow': [
      'error',
      {
        'builtinGlobals': false,
        'hoist': 'functions',
        'allow': ['resolve', 'reject', 'done', 'cb', 'state', 'options']
      }
    ],
    'max-len': [
      'error',
      120,
      {
        'ignoreTemplateLiterals': true,
        'ignoreStrings': true,
        'ignoreComments': true,
      },
    ],
    'no-underscore-dangle': [
      'error',
      {
        'allow': [
          '_id',
        ],
      },
    ],
    'import/no-extraneous-dependencies': [
      'error',
      {
        'devDependencies': ['**/*.spec.js'],
      },
    ],

    // disabled inherited rules
    'no-console': 'off',                // todo implement smarter logging
    'no-plusplus': 'off',               // only need this rule if you worry about automatic semicolon insertion
    'arrow-body-style': 'off',          // creates unnecessarily long lines or vague body scope
    'import/no-dynamic-require': 'off', // rarely used, but you'll know it when you need it
    'no-prototype-builtins': 'off',     // we are
    'no-mixed-operators': 'off',        //        all
    'no-confusing-arrow': 'off',        //            adults
    'no-param-reassign': 'off',         //                   here
    'no-bitwise': 'off',
    'import/prefer-default-export': 'off',
  },
}
