export default [
  {
    code: 'Brochure Followup',
    name: 'Brochure Follow-up',
  },
  {
    code: 'Call Lead',
    name: 'Call Lead',
  },
  {
    code: 'Call Community',
    name: 'Call Community',
  },
  {
    code: 'Email Only',
    name: 'Email Only',
  },
  {
    code: 'Tour Reminder',
    name: 'Tour Reminder',
  },
  {
    code: 'Tour Followup',
    name: 'Tour Follow-up',
  },
  {
    code: 'Reschedule Tour',
    name: 'Reschedule Tour',
  },
  {
    code: 'Verify Move-In',
    name: 'Verify Move-In',
  },
  {
    code: 'Other',
    name: 'Other',
  },
];
