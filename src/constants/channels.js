export default [
  {
    id: 'Email',
    name: 'Email',
  },
  {
    id: 'Direct Mail',
    name: 'Direct Mail',
  },
  {
    id: 'Digital',
    name: 'Digital',
  },
  {
    id: 'Other',
    name: 'Other',
  },
];
