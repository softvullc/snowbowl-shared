import channels from './channels';
import eventTypes from './eventTypes';
import familyReferences from './familyReferences';
import maritalStatuses from './maritalStatuses';
import states from './states';
import taskDescriptions from './taskDescriptions';

const keyBy = (list, key) => {
  return list.reduce((index, item) => {
    index[item[key]] = item;

    return index;
  }, {});
};

const channelsIndex = keyBy(channels, 'id');
const eventTypesIndex = keyBy(eventTypes, 'code');
const familyReferencesIndex = keyBy(familyReferences, 'id');
const maritalStatusesIndex = keyBy(maritalStatuses, 'id');
const statesIndex = keyBy(states, 'id');
const taskDescriptionsIndex = keyBy(taskDescriptions, 'code');

export {
  channels,
  eventTypes,
  familyReferences,
  maritalStatuses,
  states,
  taskDescriptions,
  channelsIndex,
  eventTypesIndex,
  familyReferencesIndex,
  maritalStatusesIndex,
  statesIndex,
  taskDescriptionsIndex,
};
