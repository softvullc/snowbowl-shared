export default [
  {
    id: 'Married',
    name: 'Married',
  },
  {
    id: 'Divorced',
    name: 'Divorced',
  },
  {
    id: 'Separated',
    name: 'Separated',
  },
  {
    id: 'Single',
    name: 'Single',
  },
  {
    id: 'Widowed',
    name: 'Widowed',
  },
  {
    id: 'Domestic Partner',
    name: 'Domestic Partner',
  },
];
