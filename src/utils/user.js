export const getFullName = ({ firstName, lastName } = {}) => [firstName, lastName].join(' ').trim();

export const getNames = (fullName = '') => {
  const names = fullName.split(' ');
  let lastName = '';
  let firstName = '';
  if (names.length) lastName = names[names.length - 1];
  if (names.length > 1) firstName = names.slice(0, names.length - 1).join(' ');

  return { firstName, lastName };
};
