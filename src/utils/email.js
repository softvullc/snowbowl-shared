// process email into something more usable
export const process = raw => typeof raw === 'string' ? raw.replace(/\s/g, '') : raw;

// throw errors if invalid email
export const validate = (email) => {
  if (typeof email !== 'string') throw new Error(`Email must be of type string, not ${typeof email}`);

  // RFC 5322 - https://emailregex.com/
  if (!/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)) {
    throw new Error(`${email} is not a valid email address`);
  }
};

// return boolean indicating email validity
export const isValid = (email) => {
  try {
    validate(email);

    return true;
  } catch (e) {
    return false;
  }
};

// process email into something more usable, throw if it's invalid
export const parse = (email) => {
  const clean = process(email);
  validate(clean);

  return clean;
};

// attempt to parse email, otherwise return null
export default (email) => {
  try {
    return parse(email);
  } catch (e) {
    return null;
  }
};
