export const parse = (s) => {
  if (typeof s !== 'string') throw new Error(`Cannot parse phone for type ${typeof s}`);

  // strip out everything that's not a number
  return s.replace(/[^\d]/g, '');
};

export const validate = (s) => {
  const parsedVal = parse(s);
  if (parsedVal.length !== 10 && parsedVal.length !== 11) throw new Error('Phone number must contain 10 or 11 digits');
};

export const isValid = (s) => {
  try {
    validate(s);

    return true;
  } catch (e) {
    return false;
  }
};

export const format = (s) => {
  if (isValid(s)) {
    const p = parse(s);

    if (p.length === 10) {
      const x = p.match(/^(\d{3})(\d{3})(\d{4})$/);

      // example: (816) 555-0000
      return `(${x[1]}) ${x[2]}-${x[3]}`;
    } else if (p.length === 11) {
      const y = p.match(/^(\d)(\d{3})(\d{3})(\d{4})$/);

      // example: 1 (816) 555-0000
      return `${y[1]} (${y[2]}) ${y[3]}-${y[4]}`;
    }
  } else if (typeof s === 'string') return s;

  return '';
};
