const locale = 'en-US';
const currencyType = 'USD';

// strings get stripped of anything that's not a number or decimal before attempted parse
export const parse = (c, parseFromString = Number.parseFloat) => {
  switch (typeof c) {
    case 'string': {
      const parsedVal = parseFromString(c.replace(/[^\d.]/g, ''));
      if (isFinite(parsedVal)) return parsedVal;
      throw new Error(`${c} is not a valid currency value`);
    }
    case 'number':
      return c;
    default:
      throw new Error(`Cannot parse currency for type ${typeof c}`);
  }
};

export const validate = (c, parseFromString = Number.parseFloat) => {
  parse(c, parseFromString); // do not return anything
};

export const isValid = (c, parseFromString = Number.parseFloat) => {
  try {
    parse(c, parseFromString);

    return true;
  } catch (e) {
    return false;
  }
};

export const format = (c, maximumFractionDigits = 2, minimumFractionDigits = 2) => {
  if (!isValid(c)) return '';

  return parse(c).toLocaleString(locale, {
    style: 'currency',
    currency: currencyType,
    maximumFractionDigits,
    minimumFractionDigits,
  });
};
