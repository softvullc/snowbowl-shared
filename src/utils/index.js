import * as Phone from './phone';
import * as Email from './email';
import * as User from './user';
import * as Currency from './currency';

export {
  Phone,
  Email,
  User,
  Currency,
};
